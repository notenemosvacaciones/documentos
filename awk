 

 ********** AWK ***************


Record: Registro = línea
Field: Campo = palabra separada por espacio

FS: Field Separator
RS: Record Separator

NR: Number of Records 
Número del registro de todos los archivos indicados, se va sumando
FNR: File Number of Records 
Número del registro original del archivo
NF: Number of Fields
Número de campos del registro

FS: Field Separator (Espacio o TAB por defecto)
RS: Record Separator ("\n")

OFS: Output Field Separator (" ").
ORS: Output Record Separator ("\n")

length: Longitud del registro, con sus espacios

awk 'patron {acción}' file >> awk '/Debian/ {print $0}' BaseAWK
Busca la palabra Debian e imprime todos los campos del archivo BaseAWK

awk 'patron {acción}' file >> awk '/Debian/ {print $1 $2}' BaseAWK
awk 'patron {acción}' file >> awk '/Debian/ {print $1$2}' BaseAWK
Busca la palabra Debian e imprime los campos 1 y 2 del archivo BaseAWK,
sin separación

awk 'patron {acción}' file >> awk '/Debian/ {print $1,$2}' BaseAWK
Busca la palabra Debian e imprime los campos 1 y 2 del archivo BaseAWK,
con un espacio

awk 'patron {acción}' file >> awk '/Debian/ {print $1"/"$2}' BaseAWK
Busca la palabra Debian e imprime los campos 1 y 2 del archivo BaseAWK,
con el caracter /

awk '{print FNR,NR,$0,NF,length}' BaseAWK BaseAWK2
Imprime El número del registo en el archivo original, el número del registro concatenado, todos los registros,
el número de campos, longitud del registro contando espacios

awk '{FS="_"}{print $2}' BaseAWK
Establece que el separador de campos será el guión bajo

awk '{RS="/"}{print $2}' BaseAWK
Establece que el separador de registros será el slash






