

### QUERY ###

modules (short name)
 (b)elongs               enumera a qué paquete pertenecen los ARCHIVOS
 (c)hanges               lista entradas de registro de cambios para ATOM
 chec(k)                 verificar sumas de control y marcas de tiempo para PKG
 (d)epends               list all packages directly depending on ATOM
 dep(g)raph              display a tree of all dependencies for PKG
 (f)iles                 list all files installed by PKG
 h(a)s                   list all packages for matching ENVIRONMENT data stored in /var/db/pkg
 (h)asuse                list all packages that have USE flag
 ke(y)words              display keywords for specified PKG
 (l)ist                  list package matching PKG
 (m)eta                  display metadata about PKG
 (s)ize                  display total size of all files owned by PKG
 (u)ses                  display USE flags for PKG
 (w)hich                 print full path to ebuild for PKG







### ARCHIVOS ###
/etc/portage/make.conf  >>> /etc/make.conf
/etc/portage/package.use
/etc/portage/package.accept_keywords

/var/lib/portage/world

 /etc/X11/xorg.conf.d/
/etc/X11/xorg.conf.d/45-lowres.conf


### PAQUETES ###
root # emerge --ask x11-drivers/xf86-input-synaptics

### EMERGE ###
* Instalar package *
root # emerge nombredelpaquete
root emerge -1 nombredelpaquete

* Actualizar *
root # emerge --sync
root # emerge -auDN @world
root # emerge --ask --update --newuse --deep --with-bdeps=y @world

root # env-update
root # source /etc/profile 

* Remover package *

root # emerge -C firefox
root # emerge --unmerge firefox
* Remover Dependencias huérfanas *
root # emerge -a --depclean



@@@@@ INSTALACIÓN @@@@



### MONTAR PARTICIONES ###
root # mkdir /mnt/funtoo
root # mount /dev/sda11 /mnt/funtoo

### INSTALAR STAGE 3 ###
root # cd /mnt/funtoo
/mnt/funtoo # wget http://build.funtoo.org/funtoo-current/x86-64bit/generic_64/stage3-latest.tar.xz

/mnt/funtoo # wget https://build.funtoo.org/1.4-release-std/x86-64bit/generic_64/stage3-latest.tar.xz
/mnt/funtoo # tar xpfv stage3-latest.tar.xz

### MONTAR PROCESOS ###
/mnt/funtoo # cd           ### Nos movemos al directorio root
root # mount -t proc /proc/ /mnt/funtoo/proc/
root # mount --rbind /sys/ /mnt/funtoo/sys/
root # mount --rbind /dev/ /mnt/funtoo/dev/

root # cp /etc/resolv.conf /mnt/funtoo/etc/

### CAMBIAMOS A CHRROT ###
root # chroot /mnt/funtoo/ /bin/bash
(chroot)  # env -i HOME=/root TERM=$TERM chroot . bash -l
(chroot) # export PS1="(chroot) $PS1"
(chroot) # clear

### DESCARGAR EL ARBOL PORTAGE ###
(chroot) # ego sync
(chroot) # emerge --sync

## CONFIGURAR EL SISTEMA ###
(chroot) # nano /etc/conf.d/keymaps * X11 Model: pc105 *
(chroot) # nano /etc/fstab
(chroot) # nano /etc/conf.d/hostname
(chroot) # ls /usr/share/zoneinfo
(chroot) # ln -sf /usr/share/zoneinfo/Europe/Madrid /etc/localtime
(chroot) # nano -w /etc/locale.gen
(chroot) # locale-gen
(chroot) # eselect locale list
(chroot) # eselect locale set *** número ***
(chroot) # nano -w /etc/env.d/02locale	*** LANG="es_ES.utf8" ***


(chroot) # nano -w /etc/portage/make.conf

		    CFLAGS="-march=amdfam10 -O2 -pipe"
            CXXFLAGS="-march=amdfam10 -O2 -pipe"
            EMERGE_DEFAULT_OPTS="--jobs 2 --load-average 2"
            INPUT_DEVICES="evdev synaptics libinput mouse keyboard"
            VIDEO_CARDS="nouveau"
		    MAKEOPTS="-j8"
		    LINGUAS="es"
		    L10N="es"
            USE="pulseaudio alsa"
            ACCEPT_LICENSE="*"
            PORTAGE_TMPDIR="/tmp"

### ASIGNAR FECHA ###
root # date
root # date MMDDhhmmYYYY

### ROOT ###
(chroot) # passwd

### USUARIO ###
(chroot) # useradd -m -g users -G wheel,audio,cdrom,usb,video  -s /bin/bash angel
(chroot) # passwd angel

### CREAR GRUPO POWER SI NO EXISTE Y AÑADIR USUARIO, PARA XFCE4 
(chroot) # groupadd power
(chroot) # gpasswd -a angel power

### ELEGIR PERFILES ###
(chroot) # epro show
(chroot) # epro list
(chroot) # epro flavor desktop
(chroot) # epro mix-ins +xfce
(chroot) # epro mix-in +gfxcard-nouveau

### SINCRONIZAR Y ACTUALIZAR
(chroot) # emerge --sync
(chroot) # emerge -auDN @world
(chroot) # emerge --ask --update --newuse --deep --with-bdeps=y @world

(chroot) # env-update
(chroot) # source /etc/profile 

### COMPROBAR KERNEL ESTÁ INSTALADO ###
(chroot) # emerge -s debian-sources

### SI KERNEL NO ESTÁ INSTALADO ###
(chroot) # emerge debian-sources

### RED ###
(chroot) # emerge --ask linux-firmware networkmanager  * net-misc/networkmanager -consolekit *
(chroot) # rc-update add NetworkManager default
(chroot) # rc-update add dhcpcd default
(chroot) # nmtui  *ncurses para configurar wiffi*

### PAQUETES OPCIONALES ###
(chroot) # emerge --ask sudo htop eix gentoolkit xterm
(chroot) # emerge --ask xdg-user-dirs * xdg-user-dirs-update *


### SALIR DESMONTAR Y REINICIAR ###
(chroot) # exit
root # cd /mnt
root # umount -lR funtoo
root # reboot


### KEYBOARD/MOUSE ###

If you have no keyboard/mouse input in x11, check if your kernel supports dev

root # zgrep EVDEV /proc/config.gz

if your output is:

CONFIG_INPUT_EVDEV=y

Install xf86-input-evdev

root # emerge xf86-input-evdev

### KEYBOARD EN ESPAÑOL ###

root # setxkbmap -print -verbose 10 * ver actual configuración *

Setting verbose level to 10
locale is C
Trying to load rules file ./rules/evdev...
Trying to load rules file /usr/share/X11/xkb/rules/evdev...
Success.
Applied rules from evdev:
rules:      evdev
model:      pc105
layout:     en
Trying to build keymap using the following components:
keycodes:   evdev+aliases(qwerty)
types:      complete
compat:     complete
symbols:    pc+es+inet(evdev)
geometry:   pc(pc105)
xkb_keymap {
	xkb_keycodes  { include "evdev+aliases(qwerty)"	};
	xkb_types     { include "complete"	};
	xkb_compat    { include "complete"	};
	xkb_symbols   { include "pc+es+inet(evdev)"	};
	xkb_geometry  { include "pc(pc105)"	};
 
root # setxkbmap -model pc105 -layout es  * poner configuración en español solo para ese sesión*



* de forma permanente, crear este archivo *
/etc/X11/xorg.conf.d/00-keyboard.conf

Section "InputClass"
        Identifier "system-keyboard"
        MatchIsKeyboard "on"
        Option "XkbLayout" "es"
        Option "XkbModel" "pc105"
        Option "XkbVariant" "winkeys"
        Option "XkbOptions" "grp:alt_shift_toggle"
EndSection




### SONIDO ###
root # emerge --ask media-sound/pulseaudio media-sound/pavucontrol media-sound/alsa-utils


### XORG ###
root # emerge --ask x11-drivers/xf86-input-synaptics
root # emerge --ask x11-drivers/xf86-video-nouveau
root # emerge --ask x11-base/xorg-server * Gentoo * root # emerge --ask x11-base/xorg-x11
root # emerge xorg-x11

### XFCE4 ###
root # emerge --ask xfce4-meta
user $ echo "exec startxfce4 --with-ck-launch" > ~/.xinitrc
root # rc-update add consolekit default
root # rc


user $ startx

### STARTX ###
Usar startx

Pruebe startx para arrancar el servidor X. startx es un guión (instalado por el paquete x11-apps/xinit) que ejecuta una sesión X, esto es, arranca el servidor X y algunas aplicaciones gráficas en él. Las aplicaciones a arrancar se deciden utilizando la siguiente lógica:

    Si hay un fichero llamado .xinitrc en su directorio personal, se ejecutarán las órdenes listadas en el mismo.

    De lo contrario leerá el valor de la variable XSESSION desde el fichero /etc/env.d/90xsession y lanzará la sesión adecuada. Los valores disponibles para XSESSION están en el fichero /etc/X11/Sessions/. Para definir una sesión por defecto para todo el sistema, lance:

    root #echo XSESSION="Xfce4" > /etc/env.d/90xsession

    Esto creará el archivo 90xsession y definirá la sesión X por defecto para que lance Xfce. 
    Recuerde lanzar env-update después de hacer cambios en 90xsession.

user $ startx
user $ startx /usr/bin/startfluxbox











net-print/cups-2.2.11-r1 -zeroconf
net-dns/avahi-0.7-r4 -gtk
x11-libs/gtk+-2.24.32-r1 -cups
















