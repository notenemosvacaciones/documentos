
NAME:   

       ls - lista del contenido del directorio

SINOPSIS:         

       ls OPCIÓN ... ARCHIVO ...

DESCRIPCIÓN:         

       Muestra información sobre los ARCHIVOS (el directorio actual por defecto si no se especifíca otro).
       Ordene las entradas alfabéticamente si ninguno de -cftuvSUX ni --sort es especificado.

       Los argumentos obligatorios para las opciones largas son obligatorios para las opciones cortas también.

OPCIÓN:

       -a , --todos
              no ignora las entradas que comienzan con . ( archivos ocultos )

       -A , - casi-todos
              no enumera implícito . y ...

       --author 
              con -l , imprime el autor de cada archivo

       -b , --escape
              imprime escapes de estilo C para caracteres no gráficos

       --block-size = SIZE 
              con -l , escala los tamaños por TAMAÑO al imprimirlos; p.ej,
              '- tamaño de bloque = M'; ver formato TAMAÑO a continuación

       -B , --ignore-backups
              no enumera las entradas implícitas que terminan en ~

       -c     con -lt : ordena y muestra ctime (hora de la última modificación
              de información sobre el estado del archivo); con -l : muestra ctime y ordena por
              nombre; de lo contrario: ordenar por ctime, el más nuevo primero

       -C      lista entradas por columnas

       --color [= CUANDO ]
              colorea la salida; CUÁNDO puede ser 'siempre' (predeterminado si
              omitido), 'auto' o 'nunca'; más información a continuación

       -d , --directorio
              enumera los directorios en sí mismos, no su contenido

       -D , --dired
              genera salida diseñada para el modo directo de Emacs

       -f     no ordena, habilitar -aU , deshabilitar -ls --color

       -F , --clasificar
              añade indicador (uno de * / => @ |) a las entradas

       --Tipo de archivo
              del mismo modo, excepto que no agregue '*'

       --format = WORD 
              en -x , comas -m , horizontal -x , long -l , single-column
               -1 , verbose -l , vertical -C

       - full-time 
              like -l --time-style = full-iso

       -g      como -l , pero no incluye propietario

       --grupo-directorios-primero
              agrupa directorios antes que archivos;

              se puede aumentar con una opción --sort , pero cualquier uso de
               --sort = none ( -U ) deshabilita la agrupación

       -G , --no-grupo
              en una lista larga, no imprima los nombres de los grupos

       -h , - legible por humanos 
              con -l y -s , tamaños de impresión como 1K 234M 2G, etc.

       --si  lo mismo, pero usa potencias de 1000 no 1024

       -H , --dereference-línea de comandos
              sigue los enlaces simbólicos enumerados en la línea de comando

       --dereference-command-line-symlink-to-dir
              sigue cada enlace simbólico de la línea de comando que apunta a un directorio

       --hide = PATRÓN
              no enumera las entradas implícitas que coincidan con el PATRÓN de shell (anulado
              por -a o -A )

       --hipervínculo [= CUÁNDO ]
              nombres de archivos de hipervínculos; CUÁNDO puede ser 'siempre' (predeterminado si
              omitido), 'auto' o 'nunca'

       --indicator-style = WORD
              anexa indicador con estilo WORD a los nombres de las entradas: ninguno
              (por defecto), barra ( -p ), de tipo de archivo ( --file-tipo ), clasificar ( -F )

       -i , --inodo
              imprime el número de índice de cada archivo

       -I , --ignore = PATRÓN
              no enumera las entradas implícitas que coincidan con el PATRÓN de shell

       -k , --kibibytes por 
              defecto a bloques de 1024 bytes para el uso del disco; usado solo con -s
              y totales por directorio

       -Utilizo      un formato de lista larga

       -L , --dereferencia
              al mostrar información de archivo para un enlace simbólico, mostrar
              información para el archivo al que hace referencia el enlace en lugar de
              el enlace en sí

       -m     ancho de relleno con una lista de entradas separadas por comas

       -n , --numeric-uid-gid 
              como -l , pero enumera los ID numéricos de usuario y grupo

       -N , - literal
              imprimir nombres de entrada sin citar

       -o     como -l , pero no incluye información de grupo

       -p , --indicator-style = barra
              añade / indicador a directorios

       -q , --hide-control-chars
              impresión ? en lugar de caracteres no gráficos

       --show-control-chars
              muestra caracteres no gráficos como están (el valor predeterminado, a menos que el programa
              es 'ls' y la salida es una terminal)

       -Q , --quote-name
              incluye los nombres de las entradas entre comillas dobles

       --quoting-style = PALABRA
              use WORD de estilo de comillas para los nombres de entrada: literal, locale,
              shell, shell-always, shell-escape, shell-escape-always, c,
              escape (anula la variable de entorno QUOTING_STYLE)

       -r , - reverso
              orden inverso al ordenar

       -R , --recursivo
              enumera subdirectorios de forma recursiva

       -s , --tamaño
              imprime el tamaño asignado de cada archivo, en bloques

       -S     ordena por tamaño de archivo, el más grande primero

       --sort = WORD 
              ordenar por PALABRA en lugar de nombre: ninguno ( -U ), tamaño ( -S ), tiempo ( -t ),
              versión ( -v ), extensión ( -X )

       --time = PALABRA
              cambia el valor predeterminado de usar tiempos de modificación; tiempo de acceso
              ( -u ): hora, acceso, uso; cambio de hora ( -c ): ctime, estado;
              hora de nacimiento: nacimiento, creación;

              con -l , WORD determina a qué hora mostrar; con --sort = time ,
              ordena por PALABRA (más reciente primero)

       --time-style = TIME_STYLE 
              formato de hora / fecha con -l ; ver TIME_STYLE a continuación

       -t     ordena por tiempo, el más nuevo primero; ver --tiempo

       -T , --tabsize = COLS
              suponga que la tabulación se detiene en cada COLS en lugar de 8

       -u     con -lt : ordena y muestra la hora de acceso; con -l : mostrar acceso
              tiempo y ordenar por nombre; de lo contrario: ordenar por hora de acceso, más reciente
              primero

       -U     no clasifica; enumerar entradas en orden de directorio

       -v     tipo natural de números (de versión) dentro del texto

       -w , --width = COLS
              establece el ancho de salida en COLS. 0 significa sin límite

       -x     enumera las entradas por líneas en lugar de por columnas

       -X     ordena alfabéticamente por extensión de entrada

       -Z , --context
              imprime cualquier contexto de seguridad de cada archivo

       -1     lista un archivo por línea. Evite '\ n' con -q o -b

       : help ayuda a mostrar esta ayuda y salir

       --version
              genera información de versión y salir

       El argumento SIZE es un entero y una unidad opcional (ejemplo: 10K es
       10 * 1024). Las unidades son K, M, G, T, P, E, Z, Y (potencias de 1024) o KB, MB, ...
       (potencias de 1000). También se pueden utilizar prefijos binarios: KiB = K, MiB = M,
       y así.

       El argumento TIME_STYLE puede ser full-iso, long-iso, iso, locale o
       + FORMATO. FORMATO se interpreta como en la fecha (1). Si FORMAT es
       FORMAT1 <newline> FORMAT2, luego FORMAT1 se aplica a archivos no recientes y
       FORMAT2 a archivos recientes. TIME_STYLE con el prefijo 'posix-' toma
       efecto solo fuera de la configuración regional POSIX. También el TIME_STYLE
       La variable de entorno establece el estilo predeterminado que se utilizará.

       El uso de colores para distinguir los tipos de archivos está deshabilitado tanto de forma predeterminada como
       con --color = nunca . Con --color = auto , ls emite solo códigos de color
       cuando la salida estándar está conectada a un terminal. Los LS_COLORS
       La variable de entorno puede cambiar la configuración. Usa los dircolors
       comando para configurarlo.

   Estado de salida:
       0 si está bien,

       1 si hay problemas menores (por ejemplo, no se puede acceder al subdirectorio),

       2 si hay problemas graves (por ejemplo, no puede acceder a la línea de comandos
              argumento).

AUTOR:        

       Escrito por Richard M. Stallman y David MacKenzie.

INFORMAR ERRORES:         

       Ayuda en línea de GNU coreutils: < https://www.gnu.org/software/coreutils/ >
       Informe cualquier error de traducción a < https://translationproject.org/team/ >

COPYRIGHT:         

       Copyright © 2020 Free Software Foundation, Inc. Licencia GPLv3 +: GNU
       GPL versión 3 o posterior < https://gnu.org/licenses/gpl.html >.
       Este es un software gratuito: puede cambiarlo y redistribuirlo.
       NO HAY GARANTÍA, en la medida permitida por la ley.

       Documentación completa < https://www.gnu.org/software/coreutils/pwd >
       o disponible localmente a través de: info '(coreutils) pwd invocation'

