MKDIR(1)                                             General Commands Manual                                             MKDIR(1)

NOMBRE
       mkdir - crea directorios

SINOPSIS
       mkdir [opciones] directorio...

       Opciones de POSIX: [-p] [-m modo]

       Opciones de GNU (en la forma más corta): [-p] [-m modo] [--verbose] [--help] [--version] [--]

DESCRIPCIÓN
       mkdir crea directorios con los nombres especificados.

       De  forma  predeterminada,  los  permisos  de  los directorios creados son 0777 (`a+rwx') menos los bits puestos a 1 en la
       umask.

OPCIONES
       -m modo, --mode=modo
              Establece los permisos de los directorios creados a modo, que puede ser  simbólico  como  en  chmod(1)  y  entonces
              emplea el modo predeterminado como el punto de partida.

       -p, --parents
              Crea  los  directorios padre que falten para cada argumento directorio.  Los permisos para los directorios padre se
              ponen a la umask modificada por `u+rwx'.  No hace caso de argumentos que  correspondan  a  directorios  existentes.
              (Así, si existe un directorio /a, entonces `mkdir /a' es un error, pero `mkdir -p /a' no lo es.)

       --verbose
              Muestra un mensaje para cada directorio creado. Esto es más útil con --parents.

OPCIONES ESTÁNDARES DE GNU
       --help Muestra un mensaje en la salida estándar sobre el modo de empleo y acaba con estado de éxito.

       --version
              Muestra en la salida estándar información sobre la versión, y luego acaba con estado de éxito.

       --     Termina con la lista de opciones.

ENTORNO
       Las variables LANG, LC_ALL, LC_CTYPE y LC_MESSAGES tienen los significados usuales.

CONFORME A
       POSIX 1003.2.

OBSERVACIONES
       Esta  página  describe mkdir según se encuentra en el paquete fileutils-4.0; otras versiones pueden diferir un poco. Envíe
       por correo electrónico correcciones y adiciones a la dirección aeb@cwi.nl.  Informe de fallos en el programa a  fileutils-
       bugs@gnu.ai.mit.edu.

GNU fileutils 4.0                                       Noviembre de 1998                                                MKDIR(1)
