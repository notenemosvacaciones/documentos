
NAME:   
      
       du - ( disk usage ) estima el uso del espacio de archivos

SINOPSIS:

       du OPCIÓN ... ARCHIVO ...
       du OPCIÓN ... --files0-from = F

DESCRIPCIÓN:         

       Resumir el uso de disco del conjunto de ARCHIVOS, de forma recursiva para directorios.

       Los argumentos obligatorios para las opciones largas son obligatorios para las opciones cortas también.

OPCIÓN:

       -0 , --nulo
              termina cada línea de salida con NUL, no nueva línea

       -a , --todos
              escribe recuentos para todos los archivos, no solo los directorios

       - tamaño aparente
              imprime tamaños aparentes, en lugar del uso del disco; Aunque el
              El tamaño aparente suele ser menor, puede ser mayor debido a
              agujeros en archivos ('dispersos'), fragmentación interna, indirecta
              bloques y similares

       -B , --block-size = TAMAÑO
              escala los tamaños por TAMAÑO antes de imprimirlos; por ejemplo, impresiones '-BM'
              tamaños en unidades de 1.048.576 bytes; ver formato TAMAÑO a continuación

       -b , --bytes 
              equivalentes a '--apparent-size --block-size = 1 '

       -c , --total
              produce un gran total

       -D , --dereference-args
              desreferencia solo los enlaces simbólicos que se enumeran en la línea de comando

       -d , --max-depth = N 
              imprime el total de un directorio (o archivo, con --todos ) solo si
              está N o menos niveles por debajo del argumento de la línea de comando;
              --max-depth = 0 es lo mismo que --summarize

       --files0-from = F
              resume el uso del disco de los nombres de archivo terminados en NUL
              especificado en el archivo F; si F es -, lea los nombres del estándar
              entrada

       -H     equivalente a --dereference-args ( -D )

       -h , - legible por humanos
              tamaños de impresión en formato legible por humanos (por ejemplo, 1K 234M 2G)

       --inodos
              enumera la información de uso del inodo en lugar del uso del bloque

       -k     como --block-size = 1K

       -L , --dereferencia
              desreferencia todos los enlaces simbólicos

       -l , --count-links
              cuenta tamaños muchas veces si está vinculado

       -m     como --block-size = 1M

       -P , --no desreferencia
              no sigas ningún enlace simbólico (este es el predeterminado)

       -S , --directores-separados
              para directorios no incluye el tamaño de los subdirectorios

       --si   como -h , pero usa potencias de 1000 no 1024

       -s , --resume
              muestra solo un total para cada argumento

       -t , --threshold = TAMAÑO
              excluye entradas menores que SIZE si es positivo, o entradas
              mayor que TAMAÑO si es negativo

       --time muestra la hora de la última modificación de cualquier archivo en el
              directorio, o cualquiera de sus subdirectorios

       --time = PALABRA
              muestra la hora como PALABRA en lugar de la hora de modificación: hora, acceso,
              uso, tiempo o estado

       --time-style = ESTILO
              muestra tiempos usando STYLE, que puede ser: full-iso, long-iso, iso,
              o + FORMATO; FORMAT se interpreta como en 'fecha'

       -X , --excluir-de = ARCHIVO
              excluye archivos que coincidan con cualquier patrón en FILE

       --exclude = PATRÓN
              excluye archivos que coincidan con PATTERN

       -x , --un sistema de archivos
              omite directorios en diferentes sistemas de archivos

       : help ayuda a mostrar esta ayuda y salir

       --version
              genera información de versión y salir

       Los valores mostrados están en unidades del primer TAMAÑO disponible de
       --block-size y DU_BLOCK_SIZE, BLOCK_SIZE y BLOCKSIZE
       Variables de entorno. De lo contrario, las unidades están predeterminadas a 1024 bytes (o
       512 si POSIXLY_CORRECT está configurado).

       El argumento SIZE es un entero y una unidad opcional (ejemplo: 10K es
       10 * 1024). Las unidades son K, M, G, T, P, E, Z, Y (potencias de 1024) o KB, MB, ...
       (potencias de 1000). También se pueden utilizar prefijos binarios: KiB = K, MiB = M,
       y así.

PATRONES:

       PATTERN es un patrón de shell (no una expresión regular). ¿El patrón ? 
       coincide con cualquier carácter, mientras que * coincide con cualquier cadena (compuesta de
       cero, uno o varios caracteres). Por ejemplo, * .o coincidirá con cualquier
       archivos cuyos nombres terminan en .o . Por tanto, el comando

              du --exclude = '*. o'

       omitirá todos los archivos y subdirectorios que terminen en .o (incluido el archivo .o sí mismo).

AUTOR: 

       Escrito por Torbjorn Granlund, David MacKenzie, Paul Eggert y Jim Meyering.

INFORMAR ERRORES:

       Ayuda en línea de GNU coreutils: < https://www.gnu.org/software/coreutils/ >
       Informe cualquier error de traducción a < https://translationproject.org/team/ >

COPYRIGHT:        

       Copyright © 2020 Free Software Foundation, Inc. Licencia GPLv3 +: GNU
       GPL versión 3 o posterior < https://gnu.org/licenses/gpl.html >.
       Este es un software gratuito: puede cambiarlo y redistribuirlo.
       NO HAY GARANTÍA, en la medida permitida por la ley.

