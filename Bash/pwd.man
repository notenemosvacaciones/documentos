
NAME:

       pwd: (print working directory ) imprime el nombre del directorio actual de trabajo

SINOPSIS:

       pwd OPCIÓN  ...

DESCRIPCIÓN:

       Imprime el nombre de archivo completo del directorio de trabajo actual.

OPCIÓN:

       -L , - lógico
              usa PWD del entorno, incluso si contiene enlaces simbólicos

       -P , --físico
              evita todos los enlaces simbólicos

       : help ayuda a mostrar esta ayuda y salir

       --version
              genera información de versión y salir

       Si no se especifica ninguna opción, se asume -P .

       NOTA: su shell puede tener su propia versión de pwd, que normalmente
       reemplaza la versión descrita aquí. Consulte su caparazón
       documentación para obtener detalles sobre las opciones que admite.

AUTOR:

       Escrito por Jim Meyering.

INFORMAR ERRORES:

       Ayuda en línea de GNU coreutils: < https://www.gnu.org/software/coreutils/ >
       Informe cualquier error de traducción a < https://translationproject.org/team/ >

COPYRIGHT:

       Copyright © 2020 Free Software Foundation, Inc. Licencia GPLv3 +: GNU
       GPL versión 3 o posterior < https://gnu.org/licenses/gpl.html >.
       Este es un software gratuito: puede cambiarlo y redistribuirlo.
       NO HAY GARANTÍA, en la medida permitida por la ley.


       Documentación completa < https://www.gnu.org/software/coreutils/pwd >
       o disponible localmente a través de: info '(coreutils) pwd invocation'

