#!/bin/bash


# Autor: Angel Repiso
# Fecha: 21-11-20
# Explicación y ejemplos de comandos en bash

clear
echo "#################################"
echo "#  Listado de comandos bash     #"
echo "#################################"




echo "Introduce el comando del que quieres informacion :" 
read coman
echo "Cargando datos..."
echo "Buscando información acerca del comando: $coman"
comando=$coman.man

if test -e $comando;
then
	cat $comando
	echo -n "Comando: $coman explicado. Fin. "
       	date +"%A, %d-%B-%Y"
	echo
else
	echo "No se encontró el comando $coman. No existe o todavía no está implementado."
	exit 1

fi	
