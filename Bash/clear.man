
NAME:   
      
       clear - borra la pantalla del terminal

SINOPSIS:        

       clear -T tipo -V -x

DESCRIPCIÓN:     

       borra su pantalla si esto es posible, incluyendo su
       búfer de retroceso (si se define la capacidad ampliada "E3").
       Busca en el entorno el tipo de terminal dado por el
       variable de entorno TERM , y luego en la base de datos terminfo para
       determinar cómo limpiar la pantalla.

       Escribe en la salida estándar. Puede redirigir el estándar
       salida a un archivo (lo que evita que clear realmente borre el
       pantalla), y luego coloque el archivo en la pantalla, borrándolo en ese punto.

OPCIONES:

       -T tipo 
            indica el tipo de terminal. Normalmente esta opción es
            innecesario, porque el valor predeterminado se toma del entornoTERM 
            variable . Si se especifica -T , las variables de shell
            LINES y COLUMNS también se ignorarán.

       -V   informa la versión de ncurses que se utilizó en este programa,
            y salidas. Las opciones son las siguientes:

       -x   no intenta borrar el búfer de retroceso del terminal usando
            la capacidad ampliada "E3".

