
NAME:

       sed - editor de flujo para filtrar y transformar texto

SINOPSIS:         

       sed OPCIÓN ... {script-sólo-si-no-otro-script} archivo-de-entrada ...

DESCRIPCIÓN:

       Sed es un editor de transmisiones. Se utiliza un editor de transmisiones para realizar
       transformaciones de texto en un flujo de entrada (un archivo o entrada de un
       tubería). Aunque en cierto modo es similar a un editor que permite
       ediciones con guión (como ed ), sed funciona haciendo solo una pasada
       la entrada (s) y, en consecuencia, es más eficiente. Pero es sed 's
       capacidad de filtrar texto en una tubería que distingue particularmente
       de otros tipos de editores.

       -n , --quiet , --silent

              suprime la impresión automática del espacio del patrón

       --depurar

              anota la ejecución del programa

       -e script, --expression = script

              agrega el script a los comandos que se ejecutarán

       -f archivo-script, --file = archivo-script

              agrega el contenido del archivo de script a los comandos que se ejecutarán

       --follow-symlinks

              sigue enlaces simbólicos cuando se procesan en su lugar

       -i [SUFIJO] , --en lugar [= SUFIJO ]

              edita archivos en su lugar (hace una copia de seguridad si se proporciona SUFIJO)

       -l N, --línea-longitud = N

              especifica la longitud de ajuste de línea deseada para el comando `l '

       --posix

              deshabilita todas las extensiones GNU.

       -E , -r , --regexp-extended

              utiliza expresiones regulares extendidas en el script (para
              portabilidad utilice POSIX -E ).

       -s , --separate

              Considera los archivos como separados en lugar de como un solo, continuo
              corriente larga.

       --sandbox

              opera en modo sandbox (deshabilita los comandos e / r / w).

       -u , --unbuffered

              carga cantidades mínimas de datos de los archivos de entrada y vacía
              los búferes de salida con más frecuencia

       -z , --null-data

              líneas separadas por caracteres NUL

       --help
              muestra esta ayuda y salir

       --version
              genera información de versión y salir

       Si no se proporciona la opción -e , --expression , -f , o --file , entonces la primera
       El argumento sin opción se toma como el script sed a interpretar. Todas
       los argumentos restantes son nombres de archivos de entrada; si no hay archivos de entrada
       especificado, entonces se lee la entrada estándar.

       Página de inicio de GNU sed: < https://www.gnu.org/software/sed/ >. Ayuda general
       utilizando software GNU: < https://www.gnu.org/gethelp/ >. Error de correo electrónico
       informa a: <bug-sed@gnu.org>.

SINOPSIS DE COMANDOS:

       Esta es solo una breve sinopsis de los comandos sed para servir como recordatorio
       a los que ya conocen sed ; otra documentación (como la
       texinfo document) se debe consultar para obtener descripciones más completas.

   Zero-dirección de `` comandos '' 
       : la etiqueta 
              de la etiqueta para b y t comandos.

       # comentario 
              El comentario se extiende hasta la siguiente nueva línea (o el final de un -e
              fragmento de guión).

       } El corchete de cierre de un bloque {}.

   Comandos de dirección cero o una dirección
       = Imprime el número de línea actual.

       a \

       text   Agrega texto , que tiene cada nueva línea incrustada precedida por un
              barra invertida.

       i \

       text   Inserta texto, que tiene cada nueva línea incrustada precedida por un
              barra invertida.

       q [ exit-code ]
              Sale inmediatamente del script sed sin procesar más
              entrada, excepto que si la impresión automática no está deshabilitada, la
              se imprimirá el espacio del patrón. El argumento del código de salida es un
              Extensión GNU.

       Q [ exit-code ]
              Sale inmediatamente del script sed sin procesar más
              entrada. Esta es una extensión GNU.

       r filename 
              Añade el texto leído desde el nombre de archivo .

       R filename 
              Añade una línea leída desde el nombre de archivo . Cada invocación del
              comando lee una línea del archivo. Esta es una extensión GNU.

   Comandos que aceptan rangos de direcciones
       {Comience un bloque de comandos (termine con a}).

       b label 
              Rama a etiqueta ; si se omite la etiqueta , bifurca al final del script.

       C \

       text   Reemplaza las líneas seleccionadas con texto , que tiene cada uno incrustado
              nueva línea precedida por una barra invertida.

       d      Elimina el espacio del patrón. Inicia el próximo ciclo.

       D      Si el espacio del patrón no contiene una nueva línea, comienza un nuevo ciclo normal
              como si se hubiera emitido el comando d. De lo contrario, elimina el texto en el
              espacio de patrón hasta la primera línea nueva y reinicia el ciclo con
              el espacio de patrón resultante, sin leer una nueva línea de
              entrada.

       h H   Copia / agrega espacio de patrón para mantener el espacio.

       g G   Copia / agrega espacio de retención al espacio del patrón.

       l     Enumera la línea actual en una forma `` visualmente inequívoca ''.

       l width
              Enumera la línea actual en una forma `` visualmente inequívoca '',
              rompiéndolo en caracteres de ancho . Esta es una extensión GNU.

       n N    Lee / agrega la siguiente línea de entrada en el espacio del patrón.

       p      Imprime el espacio del patrón actual.

       P      Imprime hasta la primera línea nueva incrustada del patrón actual
              espacio.

       s/ regexp / replacement /
              Intenta hacer coincidir regexp con el espacio del patrón. Si
              exitoso, reemplaza la parte correspondiente con reemplazo .
              El reemplazo puede contener el carácter especial y hacer referencia
              a esa parte del espacio del patrón que coincidía, y el
              escapes especiales \ 1 a \ 9 para referirse al correspondiente
              sub-expresiones coincidentes en la expresión regular .

       t label
              Si as /// ha realizado una sustitución exitosa desde el último
              se leyó la línea de entrada y desde el último comando to T, entonces
              rama para etiquetar ; si se omite la etiqueta , bifurca al final del script.

       T label
              Si ningún s /// ha realizado una sustitución satisfactoria desde la última
              se leyó la línea de entrada y desde el último comando to T, entonces
              rama para etiquetar ; si se omite la etiqueta , bifurca al final del script.
              Esta es una extensión GNU.

       w filename 
              Escribe el espacio del patrón actual en filename .

       W filename 
              Escribe la primera línea del espacio del patrón actual en filename .
              Esta es una extensión GNU.

       x      Intercambia el contenido de los espacios de espera y patrón.

       y/ source / dest /
              Translitera los caracteres en el espacio del patrón que aparecen
              en fuente al carácter correspondiente en dest .

Direcciones:
       El comando Sed se pueden dar sin direcciones, en cuyo caso
       el comando se ejecutará para todas las líneas de entrada; con una dirección, en
       cuyo caso el comando solo se ejecutará para las líneas de entrada que
       coincidir con esa dirección; o con dos direcciones, en cuyo caso el comando
       se ejecutará para todas las líneas de entrada que coincidan con el rango inclusivo
       de líneas comenzando desde la primera dirección y continuando hasta la segunda
       habla a. Tres cosas a tener en cuenta sobre los rangos de direcciones: la sintaxis es
       addr1 , addr2 (es decir, las direcciones están separadas por una coma); la línea
       que addr1 coincida siempre será aceptado, incluso si addr2 selecciona un
       línea anterior; y si addr2 es una expresión regular , no va a ser probada contra
       la línea que coincidió con addr1 .

       Después de la dirección (o rango de direcciones), y antes del comando, un !
       puede insertarse, lo que especifica que el comando solo se
       ejecutado si la dirección (o rango de direcciones) no coincide.

       Se admiten los siguientes tipos de direcciones:

       number Coincide sólo con el número de línea especificado (que incrementa
              acumulativamente en todos los archivos, a menos que se especifique la opción -s
              en la línea de comando).

        first ~ step
              Coincide con la línea de cada step con la línea first . por
              ejemplo, `` sed -n 1 ~ 2p '' imprimirá todas las líneas impares
              en el flujo de entrada, y la dirección 2 ~ 5 coincidirá con cada
              quinta línea, comenzando por la segunda.  first puede ser cero; en
              en este caso, sed opera como si fuera igual a step . (Esto es
              una extensión.)

       $ Match the last line.

       / regexp /
              Coincide con las líneas que coinciden con la expresión regular regexp. La 
              coincidencia se realiza en el espacio de patrón actual, que se puede
              modificar con comandos como `` s /// ''.

       \ c regexp c 
              Coincide con las líneas que coinciden con la expresión regular regexp . La c puede
              ser cualquier personaje.

       GNU sed también admite algunos formularios especiales de 2 direcciones:

       0, addr2 
              Comienza en el estado de "primera dirección coincidente", hasta que addr2 sea
              encontró. Esto es similar a 1, addr2 , excepto que si addr2 
              coincide con la primera línea de entrada, el formulario 0, addr2 será
              al final de su rango, mientras que la forma 1, addr2 seguirá
              estar al principio de su rango. Esto solo funciona cuando addr2
              es una expresión regular.

       addr1 , + N 
              coincidirá con addr1 y las N líneas que siguen a addr1 .

       addr1 , ~ N 
              coincidirá con addr1 y las líneas que siguen a addr1 hasta la siguiente
              línea cuyo número de línea de entrada es un múltiplo de N .

EXPRESIONES REGULARES:

       Los BRE POSIX.2 deberían ser compatibles, pero no lo son completamente porque
       de problemas de rendimiento. La secuencia \ n en una expresión regular
       coincide con el carácter de nueva línea, y de manera similar para \ a , \ t y otros
       secuencias. La opción -E cambia a usar extendido regular
       expresiones en su lugar; ha sido apoyado durante años por GNU sed, y
       ahora está incluido en POSIX.

BUGS:

       Envíe informes de errores por correo electrónico a bug-sed@gnu.org . Además, incluya el
       salida de `` sed --version '' en el cuerpo de su informe si lo hay
       posible.

AUTOR:

       Escrito por Jay Fenlason, Tom Lord, Ken Pizzini, Paolo Bonzini, Jim
       Meyering y Assaf Gordon.

       Este programa sed fue construido con el soporte de SELinux. SELinux está habilitado
       en este sistema.

       Página de inicio de GNU sed: < https://www.gnu.org/software/sed/ >. Ayuda general
       utilizando software GNU: < https://www.gnu.org/gethelp/ >. Error de correo electrónico
       informa a: <bug-sed@gnu.org>.

COPYRIGHT:

       Copyright © 2020 Free Software Foundation, Inc. Licencia GPLv3 +: GNU
       GPL versión 3 o posterior < https://gnu.org/licenses/gpl.html >.
       Este es un software gratuito: puede cambiarlo y redistribuirlo.
       NO HAY GARANTÍA, en la medida permitida por la ley.


