MV(1)                                                General Commands Manual                                                MV(1)

NOMBRE
       mv - mueve (renombra) ficheros

SINOPSIS
       mv [opción...] origen destino
       mv [opción...] origen... destino

       Opciones de POSIX: [-fi]

       Opciones de GNU (en la forma más corta): [-bfiuv] [-S sufijo] [-V {numbered,existing,simple}] [--help] [--version] [--]

DESCRIPCIÓN
       mv mueve o renombra ficheros o directorios.

       Si el último argumento nombra a un directorio existente, mv mueve cada uno de los otros ficheros a un fichero con el mismo
       nombre en ese directorio. Si no, si sólo se dan dos ficheros, renombra el primero al segundo. Es un error  que  el  último
       argumento no sea un directorio y se den más de dos ficheros.

       Así, `mv /a/x/y /b' renombraría el fichero /a/x/y a /b/y si /b fuera un directorio existente, y a /b si no lo fuera.

       Llamemos  destino al fichero al cual se va a mover un fichero dado.  Si destino existe, y o bien se ha dado la opción -i o
       bien destino no es modificable, y la entrada estándar es una terminal, y no se ha  dado  la  opción  -f,  mv  pregunta  al
       usuario  si quiere reemplazar el fichero, escribiendo una pregunta en la salida estándar de errores (stderr) y leyendo una
       respuesta desde la entrada estándar (stdin). Si la respuesta no es afirmativa, se salta ese fichero.

       Cuando tanto origen como destino están en el mismo sistema de ficheros, son el mismo fichero (sólo el nombre se cambia; el
       propietario,  permisos  y  marcas  de  tiempo  permanecen  intactos).  Cuando están en sistemas de ficheros diferentes, el
       fichero origen se copia con el nuevo nombre y luego se borra.  mv copiará el tiempo de modificación, el tiempo de  acceso,
       el  identificador  del  propietario  y  del grupo, y los permisos, si puede.  Cuando la copia del ID del propietario o del
       grupo falle, los bits setuid y setgid se limpian en la copia.

OPCIONES DE POSIX
       -f     No pide confirmación.

       -i     Pide confirmación cuando destino existe.  (En caso de que se den -f y -i, la última opción dada  es  la  que  tiene
              efecto.)

DETALLES DE GNU
       La  implementación  de  GNU  falla  (en  fileutils-3.16) en el sentido de que mv sólo puede mover ficheros regulares entre
       sistemas de ficheros distintos.

OPCIONES DE GNU
       -f, --force
              Borra los ficheros de destino existentes sin preguntar nunca al usuario.

       -i, --interactive
              Pregunta si se desean sobreescribir ficheros de destino regulares existentes. Si la respuesta no es afirmativa,  se
              pasa al siguiente fichero sin efectuar la operación.

       -u, --update
              No  mueve  un  fichero  no  directorio  que  tenga  un  destino existente con el mismo tiempo de modificación o más
              reciente.

       -v, --verbose
              Muestra el nombre de cada fichero antes de moverlo.

OPCIONES DE RESPALDO DE GNU
       Las versiones de GNU de programas como cp, mv, ln, install y patch crearán una copia de seguridad de ficheros que estén  a
       punto  de ser sobreescritos, modificados o destruidos. Que se deseen ficheros de respaldo se indica mediante la opción -b.
       Cómo deberían nombrarse se especifica con la opción -V. En el caso de que el nombre del fichero de respaldo se dé mediante
       el nombre del fichero extendido con un sufijo, este sufijo se especifica con la opción -S.

       -b, --backup
              Hace copias de respaldo de ficheros que están a punto de ser sobreescritos o borrados.

       -S SUFIJO, --suffix=SUFIJO
              Añade SUFIJO a cada fichero de respaldo creado.  Si no se especifica esta opción, se emplea el valor de la variable
              de entorno SIMPLE_BACKUP_SUFFIX.  Y si SIMPLE_BACKUP_SUFFIX no está definida, el valor predeterminado es `~'.

       -V MÉTODO, --version-control=MÉTODO
              Especifica cómo se nombran los ficheros de respaldo. El argumento MÉTODO puede ser `numbered' (o  `t'),  `existing'
              (o  `nil'),  o `never' (o `simple').  Si esta opción no se especifica, se emplea el valor de la variable de entorno
              VERSION_CONTROL.  Y si VERSION_CONTROL no está definida, el tipo predeterminado de respaldo es `existing'.

              Esta opción  corresponde  a  la  variable  de  Emacs  `version-control'.   Los  MÉTODOs  válidos  son  (se  aceptan
              abreviaciones inambiguas):

              t, numbered
                     Siempre hace respaldos numerados.

              nil, existing
                     Hace respaldos numerados de ficheros que ya los tengan, respaldos `simple's de los otros.

              never, simple
                     Siempre hace respaldos simples.

OPCIONES ESTÁNDARES DE GNU
       --help Muestra un mensaje en la salida estándar sobre el modo de empleo y acaba con código de éxito.

       --version
              Muestra en la salida estándar información sobre la versión y luego acaba con código de éxito.

       --     Termina la lista de opciones.

ENTORNO
       Las  variables  LANG,  LC_ALL,  LC_COLLATE,  LC_CTYPE  y  LC_MESSAGES  tienen los significados usuales. Para el sistema de
       versiones de GNU, las variables SIMPLE_BACKUP_SUFFIX y VERSION_CONTROL  controlan  la  nomenclatura  de  los  ficheros  de
       respaldo, como se ha descrito anteriormente.

CONFORME A
       POSIX 1003.2, excepto que las jerarquías de directorios no pueden moverse entre sistemas de ficheros.

OBSERVACIONES
       Esta  página describe mv según se encuentra en el paquete fileutils-4.0; otras versiones pueden diferir un poco. Envíe por
       correo electrónico correcciones y adiciones a la dirección aeb@cwi.nl.  Informe de fallos  en  el  programa  a  fileutils-
       bugs@gnu.ai.mit.edu.

GNU fileutils 4.0                                       Noviembre de 1998                                                   MV(1)
