
NAME:         

       cat: concatena archivos e imprime en la salida estándar (stdin 0)

SINOPSIS:         

       cat OPCIÓN ... ARCHIVO ...

DESCRIPCIÓN:         

       Concatena ARCHIVO (s) a la salida estándar (stdin 0)

       Sin ARCHIVO, o cuando ARCHIVO es -, lee la entrada estándar (stdin 0)

       -A , --show-all 
              equivalente a -vET

       -b , --number-nonblank 
              numera líneas de salida no vacías, anula -n

       -e      equivalente a -vE

       -E , - show-termina
              muestra $ al final de cada línea

       -n , - number
              numera todas las líneas de salida, también vacías

       -s , --squeeze-blank
              suprime repetidas líneas de salida vacías

       -t      equivalente a -vT

       -T , --show-tabs
              mostrar los caracteres TAB como ^ I

       -u      (ignorado)

       -v , --show-nonprinting
              usa la notación ^ y M-, excepto para LFD y TAB

       : help ayuda a mostrar esta ayuda y salir

       --version
              genera información de versión y salir

EJEMPLOS:         

       gato f - g
              Salida del contenido de f, luego entrada estándar, luego contenido de g.

       cat Copia la entrada estándar a la salida estándar.

AUTOR:       

       Escrito por Torbjorn Granlund y Richard M. Stallman.

INFORMAR ERRORES:

       Ayuda en línea de GNU coreutils: < https://www.gnu.org/software/coreutils/ >
       Informe cualquier error de traducción a < https://translationproject.org/team/ >

COPYRIGHT:

       Copyright © 2020 Free Software Foundation, Inc. Licencia GPLv3 +: GNU
       GPL versión 3 o posterior < https://gnu.org/licenses/gpl.html >.
       Este es un software gratuito: puede cambiarlo y redistribuirlo.
       NO HAY GARANTÍA, en la medida permitida por la ley.

