RM(1)                                                General Commands Manual                                                RM(1)

NOMBRE
       rm - borra ficheros o directorios

SINOPSIS
       rm [opciones] fichero...

       Opciones de POSIX: [-fiRr]

       Opciones de GNU (en la forma más corta): [-dfirvR] [--help] [--version] [--]

DESCRIPCIÓN
       rm  borra cada fichero dado.  Por lo normal, no borra directorios.  Pero cuando se da la opción -r o -R, se borra el árbol
       de directorios entero a partir del directorio especificado (y sin limitaciones en cuanto a la profundidad de  los  árboles
       de  directorio  que  pueden  borrarse con `rm -r').  Es un error que el último componente del camino de fichero sea . o ..
       (para evitar así sorpresas desagradables con `rm -r .*' o así).

       Si se da la opción -i, o si un fichero no es modificable, y la entrada estándar es una terminal, y la opción -f no  se  ha
       dado,  rm  pregunta  al  usuario  si quiere borrar realmente el fichero, escribiendo una pregunta en la salida estándar de
       errores y leyendo una respuesta desde la entrada estándar. Si la respuesta no es afirmativa, el fichero no se borra  y  se
       pasa al siguiente.

OPCIONES DE POSIX
       -f     No  pide  confirmación.  No  escribe mensajes de diagnóstico. No produce un estado de salida de error si los únicos
              errores han sido ficheros que no existen.

       -i     Pide confirmación.  (En el caso de que se den tanto -f como -i, el último que se escriba es el que tiene efecto.)

       -r or -R
              Borra recursivamente árboles de directorio.

DETALLES DE SVID
       La System V Interface Definition prohíbe el borrado del último  enlace  a  un  fichero  binario  ejecutable  que  se  esté
       ejecutando en ese momento.

DETALLES DE GNU
       La  implementación de GNU (en fileutils-3.16) está mal en el sentido de que hay un límite superior a la profundidad de las
       jerarquías de directorios que pueden borrarse. (Si fuera menester, se puede utilizar una utilidad  `deltree'  para  borrar
       árboles muy profundos.)

OPCIONES DE GNU
       -d, --directory
              Borra  directorios  con  unlink(2)  en  vez  de  con  rmdir(2), y no requiere que un directorio esté vacío antes de
              intentar desenlazarlo. Solamente funciona si uno  tiene  los  privilegios  apropiados.  Puesto  que  desenlazar  un
              directorio  provoca  que  los ficheros del directorio borrado se queden desreferenciados, es sabio hacer un fsck(8)
              del sistema de ficheros después de hacer esto.

       -f, --force
              No hace caso de los ficheros que no existan y nunca pregunta al usuario.

       -i, --interactive
              Pregunta si borrar cada fichero. Si la respuesta no es afirmativa, se pasa al siguiente fichero sin borrar éste.

       -r, -R, --recursive
              Borra los contenidos de directorios recursivamente.

       -v, --verbose
              Muestra el nombre de cada fichero antes de borrarlo.

OPCIONES ESTÁNDARES DE GNU
       --help Muestra un mensaje en la salida estándar sobre el modo de empleo y acaba con código de éxito.

       --version
              Muestra en la salida estándar información sobre la versión y luego acaba con código de éxito.

       --     Termina la lista de opciones.

ENTORNO
       Las variables LANG, LC_ALL, LC_COLLATE, LC_CTYPE y LC_MESSAGES tienen los significados usuales.

CONFORME A
       POSIX 1003.2, excepto por la limitación en la profundidad de la jerarquía de ficheros.

OBSERVACIONES
       Esta página describe rm según se encuentra en el paquete fileutils-4.0; otras versiones pueden diferir un poco. Envíe  por
       correo  electrónico  correcciones  y  adiciones  a la dirección aeb@cwi.nl.  Informe de fallos en el programa a fileutils-
       bugs@gnu.ai.mit.edu.

GNU fileutils 4.0                                       Noviembre de 1998                                                   RM(1)
