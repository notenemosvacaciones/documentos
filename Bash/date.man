
NAME:

       date: imprime o establece la fecha y hora del sistema.

SINOPSIS         arriba

       date OPCIÓN ... + FORMATO
       date [ -u | --utc | --universal ] [ MMDDhhmm [[ CC ] YY ] [ .ss ]]

DESCRIPCIÓN:

       Visualiza la hora actual en el FORMATO dado o configura la fecha del sistema.

OPCIÓN:

       Los argumentos obligatorios para las opciones largas son obligatorios para las opciones cortas
       también.

       -d , --date = CADENA
              muestra el tiempo descrito por STRING, no "ahora"

       --debug
              anota la fecha analizada y advertir sobre el uso cuestionable para stderr

       -f , --file = DATEFILE 
              como --date ; una vez por cada línea de DATEFILE

       -I [FMT] , --iso-8601 [= FMT ]
              fecha / hora de salida en formato ISO 8601. FMT = 'fecha' solo para fecha
              (el valor predeterminado), 'horas', 'minutos', 'segundos' o 'ns' para la fecha
              y el tiempo con la precisión indicada. Ejemplo:
              2006-08-14T02: 34: 56-06: 00

       -R , --rfc-email
              fecha y hora de salida en formato RFC 5322. Ejemplo: lun, 14 de agosto 2006 02:34:56 -0600

       --rfc-3339 = FMT
              fecha / hora de salida en formato RFC 3339. FMT = 'fecha', 'segundos',
              o 'ns' para fecha y hora con la precisión indicada.
              Ejemplo: 2006-08-14 02: 34: 56-06: 00

       -r , --reference = ARCHIVO
              muestra la última hora de modificación de ARCHIVO

       -s , --set = STRING
              establece el tiempo descrito por STRING

       -u , --utc , --universal
              imprime o configura la hora universal coordinada (UTC)

       : help ayuda a mostrar esta ayuda y salir

       --version
              genera información de versión y salir

       FORMAT: controla la salida. Las secuencias interpretadas son:

       %% un literal %

       %a el nombre abreviado del día de la semana de una configuración regional (p. ej., sáb)

       %A El nombre completo del día de la semana de una configuración regional (p. Ej., sábado)

       %b nombre del mes abreviado de la configuración regional (p. ej., nov)

       %B nombre completo del mes de la configuración regional (por ejemplo, noviembre)

       %c fecha y hora de la configuración regional (p. ej., jueves 3 de marzo 23:05:25 2005)

       %C siglo; como% Y, excepto que omite los dos últimos dígitos (por ejemplo, 20)

       %d día del mes (p. ej., 01)

       %D fecha; igual que% m /% d /% y

       %e día del mes, espacio relleno; igual que% _d

       %F fecha completa; como % + 4Y-% m-% d

       %g últimos dos dígitos del año del número de semana ISO (ver% G)

       %G año del número de semana ISO (ver% V); normalmente útil solo con %V

       %h igual que% b

       %H hora (00..23)

       %I hora (01..12)

       %j día del año (001..366)

       %k hora, espacio relleno (0..23); igual que %_H

       %l hora, espacio relleno (1..12); igual que %_I

       %m mes (01..12)

       %M minuto (00..59)

       %n una nueva línea

       %N nanosegundos (000000000..999999999)

       %p el equivalente local de AM o PM; en blanco si no se sabe

       %P como %p, pero en minúsculas

       %q trimestre del año (1..4)

       %r hora del reloj de 12 horas de la configuración regional (por ejemplo, 11:11:04 p.m.)

       %R hora y minuto de 24 horas; igual que %H:%M

       %s segundos desde 1970-01-01 00:00:00 UTC

       %S segundo (00..60)

       %t pestaña

       %T tiempo; igual que %H:%M:%S

       %u día de la semana (1..7); 1 es lunes

       %U número de semana del año, con el domingo como primer día de la semana (00..53)

       %V Número de semana ISO, con el lunes como primer día de la semana (01..53)

       %w día de la semana (0..6); 0 es domingo

       %W número de semana del año, con el lunes como primer día de la semana (00..53)

       %x representación de la fecha de la configuración regional (por ejemplo, 31/12/99)

       %X representación de tiempo de la configuración regional (por ejemplo, 23:13:48)

       %y últimos dos dígitos del año (00..99)

       %Y año cuatro dígitos (2020)

       %z + hhmm zona horaria numérica (p. ej., -0400 )

       %:z + hh: mm zona horaria numérica (p. ej., -04 : 00)

       %::z + hh: mm: ss zona horaria numérica (por ejemplo, -04 : 00: 00)

       %:::z zona horaria numérica con: con la precisión necesaria (p. ej., -04 ,
              +05: 30)

       %Z abreviatura alfabética de la zona horaria (por ejemplo, EDT)

       De forma predeterminada, la fecha rellena los campos numéricos con ceros. El seguimiento
       las banderas opcionales pueden seguir a '%':

       - (guion) no rellena el campo

       _ (subrayado) pad con espacios

       0 (cero) pad con ceros

       + rellena con ceros y poner '+' antes de años futuros con> 4 dígitos

       ^ usa mayúsculas si es posible

       # usa el caso opuesto si es posible

       Después de cualquier indicador viene un ancho de campo opcional, como un número decimal;
       luego un modificador opcional, que es E para usar la configuración regional
       representaciones alternativas si están disponibles, o para usar la configuración regional
       símbolos numéricos alternativos si están disponibles.

EJEMPLOS:

       Convierte los segundos desde la época (1970-01-01 UTC) a una fecha

              $ fecha --fecha = '@ 2147483647'

       Muestra la hora en la costa oeste de EE. UU. (Usa tzselect (1) para encontrar
       TZ)

              $ TZ = fecha de 'América / Los_Angeles'

       Muestra la hora local de las 9 a. M. Del próximo viernes en la costa oeste de EE. UU.

              $ date --date = 'TZ = "America / Los_Angeles" 09:00 el próximo viernes'

DATE STRING:

       --Date = STRING es una cadena de fecha legible por humanos en su mayoría de formato libre
       como "Domingo 29 de febrero de 2004 16:21:42 -0800" o "2004-02-29 16:21:42" o
       incluso "el próximo jueves". Una cadena de fecha puede contener elementos que indiquen
       fecha del calendario, hora del día, zona horaria, día de la semana, hora relativa,
       fecha relativa y números. Una cadena vacía indica el comienzo.
       del día. El formato de la cadena de fecha es más complejo de lo que es fácil
       documentado aquí, pero se describe completamente en la documentación de información.

AUTOR:

       Escrito por David MacKenzie.

INFORMAR ERRORES:

       Ayuda en línea de GNU coreutils: < https://www.gnu.org/software/coreutils/ >
       Informe cualquier error de traducción a < https://translationproject.org/team/ >

COPYRIGHT:

       Copyright © 2020 Free Software Foundation, Inc. Licencia GPLv3 +: GNU
       GPL versión 3 o posterior < https://gnu.org/licenses/gpl.html >.
       Este es un software gratuito: puede cambiarlo y redistribuirlo.
       NO HAY GARANTÍA, en la medida permitida por la ley.

