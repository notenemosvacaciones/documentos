
youtube-dl https://www.youtube.com/watch?v=iWo1cDGlNDI

## Ver formatos disponibles
youtube-dl -F https://www.youtube.com/watch?v=iWo1cDGlNDI

## Descargar en un formato específico
youtube-dl -f 22 https://www.youtube.com/watch?v=iWo1cDGlNDI

## Descargar en el mejor formato
youtube-dl -f "best" https://www.youtube.com/watch?v=iWo1cDGlNDI

## Descargar solo audio
youtube-dl -x https://www.youtube.com/watch?v=iWo1cDGlNDI

## Descargar audio en un determinado formato
youtube-dl -x --audio-format "mp3" https://www.youtube.com/watch?v=iWo1cDGlNDI

