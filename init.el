


(require 'package)
(setq package-enable-at-startup nil)

;; REPO MELPA
(add-to-list 'package-archives '("melpa" . "https://melpa.org/packages/") t)


(package-initialize)


(global-page-break-lines-mode t)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;; PACKAGE.EL
;; Instala use-package si no lo está
(unless (package-installed-p 'use-package)
	(package-refresh-contents)
	(package-install 'use-package))



;; Instala spacemacs-theme si no está instalado
;;(unless (package-installed-p 'spacemacs-theme)
;;	(package-refresh-contents)
;;	(package-install 'spacemacs-theme))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;; USE-PACKAGE


;; Instala el tema spacemacs y lo carga
;;(use-package spacemacs-theme
;;	:ensure t
;;	:defer t
;;	:init
;;  	(load-theme 'spacemacs-dark t))
;;	(provide 'theme)
;; Fin tema spacemacs


;; Instala which-key
(use-package which-key
	:ensure t
	:init
	(which-key-mode))
;; Fin which-key


;; Instala sudo-edit
(use-package sudo-edit
	:ensure t
	:init)
;; Fin sudo sudo-edit

;; Instala popup-kill-ring
;; Sirve para elegir opciones al pegar con M-y
(use-package popup-kill-ring
	:ensure t
	:bind ("M-y" . popup-kill-ring))
;; Fin popup-kill-ring


;; Instala org-bullets
;; Adorna asteriscos en org-mode
(use-package org-bullets
	:ensure t
	:hook (org-mode . org-bullets-mode))
;; Fin org-bullets


;; Instala selectrum
;; Adorna asteriscos en org-mode
(use-package selectrum
	:ensure t
	:init
	(selectrum-mode +1))
;; Fin selectrum


;; Instala el tema doom y lo carga
(use-package doom-themes
  :ensure t
  :config
  ;; Global settings (defaults)
	(setq doom-modeline-height 20) 
	(setq doom-modeline-bar-width 2)
	(setq doom-modeline-major-mode-color-icon t)
  (setq doom-themes-enable-bold t    ; if nil, bold is universally disabled
        doom-themes-enable-italic t) ; if nil, italics is universally disabled
  (load-theme 'doom-one t)

  ;; Enable flashing mode-line on errors
  (doom-themes-visual-bell-config)

  ;; Enable custom neotree theme (all-the-icons must be installed!)
  (doom-themes-neotree-config)

  ;; or for treemacs users
  (setq doom-themes-treemacs-theme "doom-atom") ; use "doom-colors" for less minimal icon theme
  (doom-themes-treemacs-config)

  ;; Corrects (and improves) org-mode's native fontification.
  (doom-themes-org-config))

;; Fin tema doom


;; Instala domm-modeline
(use-package doom-modeline
  :ensure t
  :init (doom-modeline-mode 1))


;; Instala all-the-icons
(use-package all-the-icons
 	 :ensure t)

;; Despueś ejecutar M-x all-the-icons-install-fonts
;; Se instalarán en /home/angel/.local/share/fonts/

;; Instala page-break-lines
;; Utilizado por dashboard
(use-package page-break-lines
 	 :ensure t)

;; Instala nov
;; Uti;izado para archvios .epub
(use-package nov
	:ensure t
	:init
	(add-to-list 'auto-mode-alist '("\\.epub\\'" . nov-mode))
	)


;; Instala dashboard
(use-package dashboard
	:ensure t
	:config
	(progn
		(setq dashboard-items '((recents  . 10)
                        (bookmarks . 2)
                        (projects . 2)
                        (agenda . 2)
                        (registers . 5)))
		(setq dashboard-center-content t)
		(setq dashboard-banner-logo-title "Bienvenido a Emacs")

		(setq dashboard-item-names '	(
											("Recent Files:" . "Archivos recientes:")
											("Bookmarks:" . "Marcadores:")
											("Projects:" . "Proyectos:")
											("Agenda for the coming week:" . "Agenda:")
											("Registers:" . "Registros:")
										)

		)
		(setq dashboard-set-file-icons t)
		(setq dashboard-set-heading-icons t)
		(setq dashboard-startup-banner "/home/angel/Imágenes/emacs.png")
		(setq dashboard-footer-messages '("¡Ha quedado bonito!"))
		(setq dashboard-set-init-info t)
		
	)
  (dashboard-setup-startup-hook))

;; Fin instalación dashboard


;; Instala treemacs
(use-package treemacs
  :ensure t
  :defer t
  :init
  (with-eval-after-load 'winum
    (define-key winum-keymap (kbd "M-0") #'treemacs-select-window))
  :config
  (progn
    (setq treemacs-collapse-dirs                   (if treemacs-python-executable 3 0)
          treemacs-deferred-git-apply-delay        0.5
          treemacs-directory-name-transformer      #'identity
          treemacs-display-in-side-window          t
          treemacs-eldoc-display                   t
          treemacs-file-event-delay                5000
          treemacs-file-extension-regex            treemacs-last-period-regex-value
          treemacs-file-follow-delay               0.2
          treemacs-file-name-transformer           #'identity
          treemacs-follow-after-init               t
          treemacs-expand-after-init               t
          treemacs-git-command-pipe                ""
          treemacs-goto-tag-strategy               'refetch-index
			 treemacs-indentation                     2
          treemacs-indentation-string              " " 
          treemacs-is-never-other-window           nil
          treemacs-max-git-entries                 5000
          treemacs-missing-project-action          'ask
          treemacs-move-forward-on-expand          nil
          treemacs-no-png-images                   nil
          treemacs-no-delete-other-windows         t
          treemacs-project-follow-cleanup          nil
          treemacs-persist-file                    (expand-file-name ".cache/treemacs-persist" user-emacs-directory)
          treemacs-position                        'left
          treemacs-read-string-input               'from-child-frame
          treemacs-recenter-distance               0.1
          treemacs-recenter-after-file-follow      nil
          treemacs-recenter-after-tag-follow       nil
          treemacs-recenter-after-project-jump     'always
          treemacs-recenter-after-project-expand   'on-distance
          treemacs-litter-directories              '("/node_modules" "/.venv" "/.cask")
          treemacs-show-cursor                     nil
          treemacs-show-hidden-files               t
          treemacs-silent-filewatch                nil
          treemacs-silent-refresh                  nil
          treemacs-sorting                         'alphabetic-asc
          treemacs-select-when-already-in-treemacs 'move-back
          treemacs-space-between-root-nodes        t
          treemacs-tag-follow-cleanup              t
          treemacs-tag-follow-delay                1.5
          treemacs-text-scale                      nil
          treemacs-user-mode-line-format           nil
          treemacs-user-header-line-format         nil
          treemacs-wide-toggle-width               70
          treemacs-width                           35
          treemacs-width-increment                 1
          treemacs-width-is-initially-locked       t
          treemacs-workspace-switch-cleanup        nil)

    ;; The default width and height of the icons is 22 pixels. If you are
    ;; using a Hi-DPI display, uncomment this to double the icon size.
    ;;(treemacs-resize-icons 44)

    (treemacs-follow-mode t)
    (treemacs-filewatch-mode t)
    (treemacs-fringe-indicator-mode 'always)

    (pcase (cons (not (null (executable-find "git")))
                 (not (null treemacs-python-executable)))
      (`(t . t)
       (treemacs-git-mode 'deferred))
      (`(t . _)
       (treemacs-git-mode 'simple)))

    (treemacs-hide-gitignored-files-mode nil))
  :bind
  (:map global-map
        ("M-0"       . treemacs-select-window)
        ("C-x t 1"   . treemacs-delete-other-windows)
        ("C-x t t"   . treemacs)
        ("C-x t B"   . treemacs-bookmark)
        ("C-x t C-t" . treemacs-find-file)
        ("C-x t M-t" . treemacs-find-tag)))

;; (use-package treemacs-evil
;;  :after (treemacs evil)
;;  :ensure t)

(use-package treemacs-projectile
  :after (treemacs projectile)
  :ensure t)

(use-package treemacs-icons-dired
  :hook (dired-mode . treemacs-icons-dired-enable-once)
  :ensure t)

(use-package treemacs-magit
  :after (treemacs magit)
  :ensure t)

(use-package treemacs-persp ;;treemacs-perspective if you use perspective.el vs. persp-mode
  :after (treemacs persp-mode) ;;or perspective vs. persp-mode
  :ensure t
  :config (treemacs-set-scope-type 'Perspectives))

;; Fin instalación treemacs


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;


;; OPCIONES
;; Muestra los números de las líneas a la derecha
  (global-display-line-numbers-mode)

;; asina teclas a sudo-edit
 (global-set-key (kbd "C-c C-r") 'sudo-edit)

;; Muestra el número columna actual en la barra de estado
   (column-number-mode)

;; Muestra el número de la línea actual en la barra de estado
	(line-number-mode)

;; Muestra la hora en la barra
(display-time)

;; Muestra el tamaño del archivo en la barra de estado
(size-indication-mode)

;; Resaltamos la línea en la que estamos posicionados
(global-hl-line-mode 1)

;; Inicia Emacs en pantalla maximizada
(add-to-list 'default-frame-alist '(fullscreen . maximized))

;; Cambia fuente y tamaño
(add-to-list 'default-frame-alist
             '(font . "DejaVu Sans Mono-14"))


;; Activanos el modo ido de forma predeterminada
;; (setq ido-enable-flex-matching t)
;; (setq ido-everywhere t)
;; (ido-mode 1)

;; Mustra la ruta completa del archivo en ventana
(setq-default frame-title-format '("%f [%m]"))

;; Resalta el par correspondiente de paréntesis
(show-paren-mode 1)

;; Margen derecho de 10 pixeles
(set-fringe-mode 10)

;; Quitamos la barra de scroll
(scroll-bar-mode -1)

;; Indentación para org-mode
(add-hook 'org-mode-hook 'org-indent-mode)

;; ver PDF continuo
 
(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(doc-view-continuous t)
 '(package-selected-packages
   '(use-package which-key spacemacs-theme popup-kill-ring org-bullets selectrum doom-themes doom-modeline 
all-the-icons page-break-lines nov dashboard treemacs treemacs-evil treemacs-projectile treemacs-icons-dired 
treemacs-magit treemacs-persp sudo-edit)))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;; ESCRITO POR EMACS

(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 )
