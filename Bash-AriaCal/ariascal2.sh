#!/bin/bash

# Funcion Inicio
	Inicio () {

echo		
echo " ################################"
echo " # 0.- Salir                    #"
echo " # 1.- Nuevo empleado           #"
echo " # 2.- Eliminar empleado        #"
echo " # 3.- Día Trabajado            #"
echo " # 4.- Día Descansado           #"
echo " # 5.- Eliminar Día Trabajado   #"
echo " # 6.- Eliminar Día Descansado  #"
echo " # 7.- Listar empleados         #"
echo " # 8.- Listar Días Trabajados   #"
echo " ################################"
echo
echo " Introduce una opción: "
read opcion

# Escoger opciones
case $opcion in

	0)
      	clear
	echo " Saliendo..."
	sleep 2
	clear
	exit 0 
	;;

	1)
    	Nuevo_empleado
    	;;   

	2)
	Eliminar_empleado
	;;

	3)
	Dia_trabajado
	;;

	4)
	Dia_descansado
	;;

	7)
	Listar_empleados
	;;


	*)
	 echo " Esa opción no existe."
	 echo " Inténtalo de nuevo"
	 sleep 2
	 clear
	 Inicio
	 ;;
esac

}
# Fin Funcion Inicio

#####################################################


# Funcion Comprobar_empleado
Comprobar_empleado () {

echo "Hola"
}           
# Fin Funcion Comprobar_empleado


#####################################################


# Funcion Nuevo_empleado (1)			

Nuevo_empleado () {
clear
echo " Introduce nombre del nuevo empleado: "
read empleado

# Comprobamos si existe ese empleado
if test -d $HOME/AriasCal/$empleado
then
	clear
	echo " El empleado: $empleado ya existe."
	sleep 3
	clear
	Inicio
else
	mkdir $HOME/AriasCal/$empleado
	touch $HOME/AriasCal/$empleado/dt.abd
	touch $HOME/AriasCal/$empleado/dd.abd
	echo " Empleado: $empleado creado."
	sleep 2
	clear
	Inicio
fi

			}
# Fin Funcion Nuevo_empleado (1)

#####################################################


# Funcion Eliminar_empleado (2)
Eliminar_empleado () {
clear
echo " Introduce nombre del empleado a eliminar: "
read eliminar


# Comprobamos si existe el empleado
if test -d $HOME/AriasCal/$eliminar
	then
		rm -R $HOME/AriasCal/$eliminar
		echo " Eliminando..."
		echo " Eliminado: $eliminar"
		sleep 2
		clear
		Inicio

	else
		echo " Empleado: $eliminar no existe"
		sleep 2
		clear
		Inicio

fi

}
# Fin Funcion Eliminar_empleado (2)


#####################################################


# Funcion Dia_trabajado (3)
Dia_trabajado () {
clear
echo " Día trabajado"
echo " Nombre del empleado: "
read nom

# Comprobamos si existe ese empleado

# Si existe, ponemos la fecha
if test -d $HOME/AriasCal/$nom
	then
clear
echo " Día trabajado"
echo " Introduce el día: "
read dia
echo " Introduce el mes: "
read mes
echo " Introduce año: "
read year

fecha=$year-$mes-$dia


       # Comprobamos que la fecha es correcta
	if date -d $fecha >/dev/null 2>&1;
	then
	echo "Fecha: "$(date -d $fecha +'%A, %d-%B-%Y')
	echo $(date -d $fecha +'%A, %d-%B-%Y') >> $HOME/AriasCal/$nom/dt.abd
	echo "Trabajado guardado correctamente"
	sleep 2
	clear
	Inicio
	
       # No lo es volvemos al Inicio
	else
    	echo " La fecha no es válida. Introduce otra."
	sleep 2
	clear
	Inicio

	fi
	
	


# Si no existe el empleado volvemos al Inicio sin hacer nada	
else
	echo " Empleado: $nombre no existe."
	echo " Introduce un nombre correcto."
	sleep 2
	clear
	Inicio

fi
		}
# Fin Funcion Dia_trabajado (3)


#####################################################


# Funcion Dia_descansado (4)
Dia_descansado () {
clear
echo " Día descansado:"
echo " Nombre del empleado: "
read nombre

# Comprobamos si existe ese empleado

# Si existe, ponemos la fecha
if test -d $HOME/AriasCal/$nombre
	then
clear
echo " Día descansado:"
echo " Introduce el día: "
read dia
echo " Introduce el mes: "
read mes
echo " Introduce año: "
read year

fecha=$year-$mes-$dia


       # Comprobamos que la fecha es correcta
	if date -d $fecha >/dev/null 2>&1;
	then
	echo "Fecha: "$(date -d $fecha +'%A, %d-%B-%Y')
	echo $(date -d $fecha +'%A, %d-%B-%Y') >> $HOME/AriasCal/$nombre/dd.abd
	echo "Descansado guardado correctamente"
	sleep 2
	clear
	Inicio
	
       # No lo es volvemos al Inicio
	else
    	echo " La fecha no es válida. Introduce otra."
	sleep 2
	clear
	Inicio

	fi
	
	


# Si no existe el empleado volvemos al Inicio sin hacer nada	
else
	echo " Empleado: $nombre no existe."
	echo " Introduce un nombre correcto."
	sleep 2
	clear
	Inicio

fi
		}
# Fin Funcion Dia_descansado (4)


#####################################################


# Funcion Listar_empleados (7)	
Listar_empleados () {
clear
echo " Empleados:"
cd $HOME/AriasCal/
ls -d */ | cut -f1 -d'/'
echo
echo
echo
cd $HOME
Inicio

		}
# Fin Funcion Listar_empleados (7)

#####################################################




# Inicio del programa	

# Ruta inicial
cd $HOME

# Si no existe la carpeta base AriasCal se crea
if test -d AriasCal
	then
	        clear	
		echo " Todo en orden..."
		sleep 2	
		Inicio

	else 
		clear
		echo " Creando directorios.."
		mkdir AriasCal
		echo " Hecho."
		sleep 2
		Inicio	     
	fi

exit 0






