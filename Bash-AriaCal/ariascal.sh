#!/bin/bash

# Pedimos contraseña y la convertimos en asteriscos

echo
PROMPT="Introduzca su password: "
while IFS= read -p "$PROMPT" -r -s -n 1 char; do
    if [[ $char == $'\0' ]]; then
        break
    fi
    PROMPT='*'
    SECRET_PASSWD+="$char"
done
echo

# Si es incorrecta salimos
if [ $SECRET_PASSWD != "actm" ];
then
        echo " Contaseña incorrecta"
	echo " Saliendo..."
	sleep 2
	exit 0

fi


# Funcion Inicio
	Inicio () {

echo		
echo " ################################"
echo " # 0.- Salir                    #"
echo " # 1.- Editar Empleados         #"
echo " # 2.- Listar Empleados         #"
echo " # 3.- Nuevo Empleado           #"
echo " ################################"
echo
echo " Introduce una opción: "
read opcion

# Escoger opciones
case $opcion in

	0)
      	clear
	echo " Saliendo..."
	sleep 2
	clear
	exit 0 
	;;

	1)
	clear
    	Editar_empleados
    	;;   

	2)
	Listar_empleados
	;;

	3)
	Nuevo_empleado
	;;

	*)
	 echo " Esa opción no existe."
	 echo " Inténtalo de nuevo"
	 sleep 2
	 clear
	 Inicio
	 ;;
esac

}
# Fin Funcion Inicio

#####################################################

# EDITAMOS EMPLEADOS
# Funcion Comprobar_empleado (1)
Editar_empleados () {

clear
echo " Introduce nombre del empleado a editar: "
read editar

# Comprobamos si existe el empleado
if test -d $HOME/AriasCal/$editar
	then
	Inicio2 $editar	

	else
		echo " Empleado: $editar no existe"
		sleep 2
		clear
		Inicio
fi
}
           
# Fin Funcion Comprobar_empleado (1)


#####################################################



# Funcion Listar_empleados (2)
# Se crea el archivo empleados

Listar_empleados () {
clear
touch AriasCal/empleados.txt
echo " Empleados:"
cat -n  $HOME/AriasCal/empleados.txt
# ls -d */ | cut -f1 -d'/'
echo
echo
echo
cd $HOME
Inicio

		}
# Fin Funcion Listar_empleados (2)


#####################################################



# Funcion Nuevo_empleado (3)			

Nuevo_empleado () {
clear
echo " Introduce nombre del nuevo empleado: "
read empleado

# Comprobamos si existe ese empleado
if test -d $HOME/AriasCal/$empleado
then
	clear
	echo " El empleado: $empleado ya existe."
	sleep 3
	clear
	Inicio
else
	mkdir $HOME/AriasCal/$empleado
	touch $HOME/AriasCal/$empleado/dt.abd
	touch $HOME/AriasCal/$empleado/dd.abd
	echo $empleado >> $HOME/AriasCal/empleados.txt
	echo " Empleado: $empleado creado."
	sleep 2
	clear
	Inicio
fi

			}
# Fin Funcion Nuevo_empleado (3)

#####################################################


############### INICIO2  ############################


# Funcion Inicio2
	Inicio2 () {
echo " Editar empleado: $1"
echo		
echo " ################################"
echo " # 0.- Salir                    #"
echo " # 1.- Vover                    #"
echo " # 2.- Eliminar empleado        #"
echo " # 3.- Día Trabajado            #"
echo " # 4.- Día Descansado           #"
echo " # 5.- Eliminar Día Trabajado   #"
echo " # 6.- Eliminar Día Descansado  #"
echo " # 7.- Listar Días Trabajados   #"
echo " # 8.- Listar Días Descansados  #"
echo " ################################"
echo
echo " Introduce una opción: "
read opcion

# Escoger opciones
case $opcion in

	0)
      	clear
	echo " Saliendo..."
	sleep 2
	clear
	exit 0 
	;;

	1)
    	clear
	Inicio
    	;;   

	2)
	Eliminar_empleado $1
	;;
	
	3)
	Dia_trabajado $1
	;;

	4)
	Dia_descansado $1
	;;

	7)
	Listar_dt $1
	;;

	8)
	Listar_dd $1
	;;

	*)
	 echo " Esa opción no existe."
	 echo " Inténtalo de nuevo"
	 sleep 2
	 clear
	 Inicio2
	 ;;
esac
}
# Fin Funcion Inicio2

#####################################################




# Funcion Eliminar_empleado (2)
Eliminar_empleado () {
# Eliminamos el empleado y el nombre del archivo
		rm -R $HOME/AriasCal/$1
		echo " Eliminando..."
		echo " Eliminado: $1"
		sed -i "/$1/d" $HOME/AriasCal/empleados.txt
		sleep 2
		clear
		Inicio

}
# Fin Funcion Eliminar_empleado (2)


#####################################################


# Funcion Dia_trabajado (3)
Dia_trabajado () {

clear
echo " Empleado: $1"
echo " Día trabajado"
echo " Introduce el día: "
read dia
echo " Introduce el mes: "
read mes
echo " Introduce año: "
read year

fecha=$year-$mes-$dia


       # Comprobamos que la fecha es correcta
	if date -d $fecha >/dev/null 2>&1;
	then
	echo "Fecha: "$(date -d $fecha +'%A, %d-%B-%Y')
	echo $(date -d $fecha +'%A, %d-%B-%Y') >> $HOME/AriasCal/$1/dt.abd
	echo "Trabajado guardado correctamente"
	sleep 2
	clear
	Inicio2 $1
	
       # No lo es volvemos al Inicio
	else
    	echo " La fecha no es válida. Introduce otra."
	sleep 2
	clear
	Inicio2 $1

	fi

		}
# Fin Funcion Dia_trabajado (3)


#####################################################


# Funcion Dia_descansado (4)
Dia_descansado () {
clear
echo " Empleado: $1"
echo " Día descansado:"
echo " Introduce el día: "
read dia
echo " Introduce el mes: "
read mes
echo " Introduce año: "
read year

fecha=$year-$mes-$dia


       # Comprobamos que la fecha es correcta
	if date -d $fecha >/dev/null 2>&1;
	then
	echo "Fecha: "$(date -d $fecha +'%A, %d-%B-%Y')
	echo $(date -d $fecha +'%A, %d-%B-%Y') >> $HOME/AriasCal/$1/dd.abd
	echo "Descansado guardado correctamente"
	sleep 2
	clear
	Inicio2 $1
	
       # No lo es volvemos al Inicio
	else
    	echo " La fecha no es válida. Introduce otra."
	sleep 2
	clear
	Inicio2 $1

	fi
	

		}
# Fin Funcion Dia_descansado (4)


#####################################################


# Funcion Listar_dt (7)
Listar_dt () {
clear
echo " Empleado: $1"
echo " Días trabajados:"
cat -n $HOME/AriasCal/$1/dt.abd
Inicio2 $1

	
	}
# Fin Funcion Listar_dt (7)



#####################################################

# Inicio del programa	

# Ruta inicial
cd $HOME

# Si no existe la carpeta base AriasCal se crea
if test -d AriasCal
	then
	        clear	
		echo " Todo en orden..."
		sleep 2	
		Inicio

	else 
		clear
		echo " Creando directorios.."
		mkdir AriasCal
		echo " Hecho."
		sleep 2
		Inicio	     
	fi

exit 0






